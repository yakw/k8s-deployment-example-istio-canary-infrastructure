# K8s Deployment Example Canary Infrastructure

Simple example of Istio Kubernetes Canary Deployment.

This uses: https://gitlab.com/wuestkamp/k8s-deployment-example-app

Medium article: https://itnext.io/kubernetes-istio-canary-deployment-5ecfd7920e1c?source=friends_link&sk=2be48393ac175a2199bf5d486cb91acf

## Install Example App yourself

To configure the example application we use here today you can do:

### Create cluster
If you use Gcloud you can do:
```
gcloud container clusters create canary --num-nodes 3 --zone europe-west3-b --machine-type n1-standard-4
```

The default k8s resource requests of all Istio components combined are about 8G memory and 4 CPU. You can adjust these though depending on your cluster needs.

### Clone Repo
```
git clone git@gitlab.com:wuestkamp/k8s-deployment-example-istio-canary-infrastructure.git
```

### Install App and Istio
Istio is already included in the repo as yaml under i/k8s/istio-system.yaml. The yaml has been generated with:
```
istioctl manifest generate --set values.kiali.enabled=true --set values.tracing.enabled=true --set values.grafana.enabled=false > i/k8s/istio-system.yaml
```

To install you can simply do:

```
cd k8s-deployment-example-istio-canary-infrastructure
kubectl apply -f i/k8s
sleep 60
kubectl get pod --all-namespaces # check no pods are pending!
```

No we need to delete all pods in default namespace so these will be created again the the Istio sidecars injected:

```
k delete pod --all
```

